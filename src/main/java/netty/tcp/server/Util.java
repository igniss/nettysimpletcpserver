package netty.tcp.server;

import io.netty.buffer.ByteBuf;
import netty.tcp.server.protocol.enums.ProtocolType;

import java.net.SocketAddress;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Util {

    static void log(String message){
        System.out.println(message);
    }

    static void log(SocketAddress socketAddress, String message){
        System.out.println("< " + socketAddress + " > @ " + LocalDateTime.now().format(DateTimeFormatter.ISO_DATE_TIME) + " : " + message);
    }

    public static boolean checkBytePosition(byte b, int position) {
        return !(b << ~position < 0);
    }

    public static byte[] dateTimeToBytes(LocalDateTime dateTime) {
        return new byte[]{
                (byte) dateTime.getYear(),
                (byte) dateTime.getMonthValue(),
                (byte) dateTime.getDayOfMonth(),
                (byte) dateTime.getHour(),
                (byte) dateTime.getMinute(),
                (byte) dateTime.getSecond()
        };
    }

    public static short generateErrorCheck(ByteBuf out) {
        return (short) Checksum.crc16(Checksum.CRC16_X25,
                out.nioBuffer(2, out.writerIndex() - 2));
    }

    public static void writeEndOfPacket(ByteBuf out) {
        out.writeShort(generateErrorCheck(out));
        out.writeShort(ProtocolType.FOOTER);
    }

}
