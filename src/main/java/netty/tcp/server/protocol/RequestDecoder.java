package netty.tcp.server.protocol;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufUtil;
import io.netty.channel.ChannelHandlerContext;
import netty.tcp.server.SimpleTCPChannelHandler;
import netty.tcp.server.Util;
import netty.tcp.server.protocol.data.RequestData;
import netty.tcp.server.protocol.data.RequestLocationData;
import netty.tcp.server.protocol.data.alarm.RequestAlarmData;
import netty.tcp.server.protocol.data.command.RequestCommandData;
import netty.tcp.server.protocol.data.heartbeat.RequestHeartbeatData;
import netty.tcp.server.protocol.data.information.RequestInformationData;
import netty.tcp.server.protocol.data.login.RequestLoginData;
import netty.tcp.server.protocol.enums.AlarmType;
import netty.tcp.server.protocol.enums.GPSSignalType;
import netty.tcp.server.protocol.enums.ProtocolType;

import java.nio.charset.StandardCharsets;

public class RequestDecoder {

    /**
     * Decode protocol type and serial number of the scooter from request.
     *
     * @param ctx ChannelHandlerContext
     * @param in  ByteBuffer
     * @return {@link RequestData}
     */
    public static RequestData decode(ChannelHandlerContext ctx, ByteBuf in) {

        short header = in.readShort();
        short length;
        ProtocolType protocol;

        if (ProtocolType.isClassicHeader(header)) {
            length = in.readUnsignedByte();
            protocol = ProtocolType.getProtocolTypeById(in.readByte() & 0xff);
            return readData(ctx, length, protocol, in);
        }

        if (ProtocolType.isExtendedHeader(header)) {
            length = in.readShort();
            protocol = ProtocolType.getProtocolTypeById(in.readByte() & 0xff);
            return readData(ctx, length, protocol, in);
        }

        return null;
    }

    private static RequestData readData(ChannelHandlerContext ctx, short length, ProtocolType protocol, ByteBuf in) {
        switch (protocol) {
            case LOGIN:
                return readLoginData(length, in);
            case HEARTBEAT:
                return readHeartbeatData(ctx, length, in);
            case LOCATION:
                return readLocationData(length, in);
            case INFORMATION:
                return readInformationData(length, in);
            case COMMAND_RESPONSE:
                //return readCommandResponseData(length, in);
                return null;
            case ALARM:
                return readAlarmData(length, in);
        }
        return null;
    }

    private static RequestLoginData readLoginData(short length, ByteBuf in) {
        String imei = ByteBufUtil.hexDump(in.readSlice(8)).substring(1);
        short model = in.readShort();
        in.skipBytes(2); //Skip 2 bytes (timezone)
        short serialNumber = in.readShort();
        short errorCheck = in.readShort();
        return new RequestLoginData(length, serialNumber, errorCheck, imei, model);
    }

    private static RequestHeartbeatData readHeartbeatData(ChannelHandlerContext ctx, short length, ByteBuf in) {

        byte info = in.readByte(); // 1B
        boolean defense = !Util.checkBytePosition(info, 0);
        boolean charging = Util.checkBytePosition(info, 2);
        boolean gps = Util.checkBytePosition(info, 6);

        double voltage = ((double) (int) in.readShort() / 100); // 2B
        GPSSignalType signalType = GPSSignalType.getGPSSignalTypeByProtocolCode(in.readByte()); // 1B

        in.skipBytes(2); //Skip 2 bytes (language)

        short sentSerialNumber = in.readShort(); // 2B (serial number that does not work properly)
        short errorCheck = in.readShort(); // 2B

        Short serialNumber = SimpleTCPChannelHandler.scooterSerialNumbers.get(ctx.channel());

        return new RequestHeartbeatData(length, serialNumber, sentSerialNumber, errorCheck, charging, gps, defense, voltage, signalType);
    }

    private static RequestLocationData readLocationData(short length, ByteBuf in) {
        in.skipBytes(6); //Skip 6 bytes (datetime)

        int gpsLength = in.readByte();
        if (gpsLength != 12) return null;

        in.skipBytes(1); //Skip 1 byte (GPS satellites)

        float latitude = ((float) in.readInt() / 1800000);
        float longitude = ((float) in.readInt() / 1800000);

        int speed = in.readByte();

        in.skipBytes(40); //Skip 40 bytes (Other data)

        short serialNumber = in.readShort();
        short errorCheck = in.readShort();

        return new RequestLocationData(length, serialNumber, errorCheck, latitude, longitude, speed);
    }

    private static RequestAlarmData readAlarmData(short length, ByteBuf in) {
        in.skipBytes(10); //Skip 6 bytes (datetime)

        int status = in.readByte() & 0xff;

        short serialNumber = in.readShort();
        short errorCheck = in.readShort();

        return new RequestAlarmData(length, serialNumber, errorCheck, AlarmType.getAlarmTypeByCode(status));
    }

    private static RequestInformationData readInformationData(short length, ByteBuf in) {
        String imei = null;
        double batteryBalance = 0.0;
        double batteryVoltage = 0.0;
        int batteryPercentage = 0;
        double currentOutputVoltage = 0.0;
        double speed = 0.0;
        boolean braking = false;
        boolean light = false;
        boolean rearLight = false;
        boolean charging = false;
        boolean disabled = false;


        int moduleNumber = 0;
        short moduleLength = 0;

        try {
            for (int i = 0; i < 15; i++) {
                moduleNumber = in.readByte();
                moduleLength = in.readShort();
                ByteBuf data = in.readSlice(moduleLength);
                if (moduleNumber == 0x00) {
                    imei = ByteBufUtil.hexDump(data).substring(1);
                } else if (moduleNumber == 0x13) {
                    //batteryBalance = ((double) data.readByte() / 100);

                    batteryVoltage = ((double) data.readInt() / 100);
                    currentOutputVoltage = ((double) data.readShort() / 100);
                    speed = ((double) data.readShort() / 100);
                    batteryPercentage = data.readByte();
                } else if (moduleNumber == 0x14) {
                    data.skipBytes(1);
/*                for (int it = 0; it < 8; it++) {
                    System.out.println(it + " - " + Util.checkBytePosition(b1, it));
                }*/
                    byte b = data.readByte();
                    rearLight = !Util.checkBytePosition(b, 1);
                    braking = !Util.checkBytePosition(b, 2);
                    light = !Util.checkBytePosition(b, 3);
                    charging = Util.checkBytePosition(b, 4);
                    disabled = Util.checkBytePosition(b, 6);
                }
            }
        } catch (Exception e) {
            return null;
        }

        return new RequestInformationData(length, (short)-1, (short)-1,
                imei, light, rearLight, charging, braking, speed, disabled, batteryVoltage, batteryPercentage, currentOutputVoltage, batteryBalance);
    }

    private static RequestCommandData readCommandResponseData(short length, ByteBuf in) {
        in.skipBytes(5); //Skip 6 bytes (datetime)

        String command = in.readBytes(length - 4 - 1 - 2 - 2).toString(StandardCharsets.US_ASCII);

        short serialNumber = in.readShort();
        short errorCheck = in.readShort();

        return new RequestCommandData(length, serialNumber, errorCheck, command);
    }
}
