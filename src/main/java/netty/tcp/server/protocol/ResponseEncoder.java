package netty.tcp.server.protocol;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import netty.tcp.server.Util;
import netty.tcp.server.protocol.data.ResponseData;
import netty.tcp.server.protocol.data.command.ResponseCommandData;
import netty.tcp.server.protocol.data.login.ResponseLoginData;

public class ResponseEncoder {

    public static ByteBuf encode(ResponseData msg) {
        ByteBuf out = Unpooled.buffer();

        out.writeShort(msg.getHeader());
        out.writeByte(msg.getLength());
        out.writeByte(msg.getProtocol().getProtocolId());

        if (msg instanceof ResponseLoginData) {
            writeLoginData(out, (ResponseLoginData) msg);
        } else if (msg instanceof ResponseCommandData) {
            writeCommandData(out, (ResponseCommandData) msg);
        }

        out.writeShort(msg.getSerialNumber());
        Util.writeEndOfPacket(out);

        return out;
    }


    private static void writeLoginData(ByteBuf out, ResponseLoginData msg) {
        out.writeBytes(Util.dateTimeToBytes(msg.getDateTime()));
    }

    private static void writeCommandData(ByteBuf out, ResponseCommandData msg) {
        out.writeByte(msg.getCommand().length() + 4);
        out.writeInt(0);
        out.writeBytes(msg.getCommand().getBytes());
        out.writeShort(0x02);
    }
}
