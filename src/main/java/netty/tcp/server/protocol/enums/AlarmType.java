package netty.tcp.server.protocol.enums;

public enum AlarmType {

    LOCK_REPORT(0xA0),
    UNLOCK_REPORT(0xA1),
    LOW_INTERNAL_BATTERY_ALARM(0xA2),
    LOW_BATTERY_AND_SHUTDOWN_ALARM(0xA3),
    ABNORMAL_ALARM(0xA4),
    ABNORMAL_UNLOCKING_ALARM(0xA5),
    SCOOTER_ALARM_ON(0xA7),
    SCOOTER_ALARM_OFF(0xA8),
    SCOOTER_MOTOR_DISCONNECTED_ALARM(0x13),
    UNKNOWN(-1);

    private final int protocolCode;

    AlarmType(int protocolCode) {
        this.protocolCode = protocolCode;
    }

    public int getProtocolCode() {
        return protocolCode;
    }

    //Helpers
    public static AlarmType getAlarmTypeByCode(int code) {
        for (AlarmType alarmType : AlarmType.values())
            if (alarmType.getProtocolCode() == code) return alarmType;
        return AlarmType.UNKNOWN;
    }

}
