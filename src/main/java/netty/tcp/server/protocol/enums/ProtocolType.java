package netty.tcp.server.protocol.enums;

public enum ProtocolType {

    //Unknown data
    UNKNOWN(-1, -1),

    //Data
    LOGIN(0x01, 11),
    HEARTBEAT(0x23, 5),
    LOCATION(0x32, 5),
    ALARM(0x33, 5),
    COMMAND(0x80, 12),
    COMMAND_RESPONSE(0x21, 10),
    INFORMATION(0x98, 6),
    METRICS(0xFD, -1);

    //Headers
    public static final short CLASSIC_HEADER = 0x7878;
    public static final short EXTENDED_HEADER = 0x7979;
    public static final short FOOTER = 0x0D0A;
    private final int protocolId;
    private final int responsePacketLength;
    ProtocolType(int protocolId, int responsePacketLength) {
        this.protocolId = protocolId;
        this.responsePacketLength = responsePacketLength;
    }

    //Helpers
    public static ProtocolType getProtocolTypeById(int id) {
        for (ProtocolType protocolType : ProtocolType.values())
            if (protocolType.getProtocolId() == id) return protocolType;
        return ProtocolType.UNKNOWN;
    }

    public static boolean isClassicHeader(int header) {
        return CLASSIC_HEADER == header;
    }

    public static boolean isExtendedHeader(int header) {
        return EXTENDED_HEADER == header;
    }

    public static boolean isFooter(int footer) {
        return FOOTER == footer;
    }

    public int getProtocolId() {
        return protocolId;
    }

    public int getResponsePacketLength() {
        return responsePacketLength;
    }
}
