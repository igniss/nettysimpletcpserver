package netty.tcp.server.protocol.enums;


public enum GPSSignalType {

    NO("Bez signálu", 0x00),
    EXTREMELY_WEAK("Extrémně slabý signál", 0x01),
    VERY_WEAK("Velmi slabý signál", 0x02),
    GOOD("Dobrý signál", 0x03),
    STRONG("Silný signál", 0x04),
    UNKNOWN("Neznámý signál", -1);

    private final String name;
    private final int protocolCode;

    GPSSignalType(String name, int protocolCode) {
        this.name = name;
        this.protocolCode = protocolCode;
    }

    public String getName() {
        return name;
    }

    public int getProtocolCode() {
        return protocolCode;
    }

    //Helpers
    public static GPSSignalType getGPSSignalTypeByProtocolCode(int code) {
        for (GPSSignalType gpsSignalType : GPSSignalType.values())
            if (gpsSignalType.getProtocolCode() == code) return gpsSignalType;
        return GPSSignalType.UNKNOWN;
    }

}
