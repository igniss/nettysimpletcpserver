package netty.tcp.server.protocol.data.information;

import netty.tcp.server.protocol.enums.ProtocolType;
import netty.tcp.server.protocol.data.ResponseData;

public class ResponseInformationData extends ResponseData {

    public ResponseInformationData(short serialNumber) {
        super(ProtocolType.EXTENDED_HEADER, (short) ProtocolType.INFORMATION.getResponsePacketLength(),
                ProtocolType.INFORMATION, serialNumber);
    }

}