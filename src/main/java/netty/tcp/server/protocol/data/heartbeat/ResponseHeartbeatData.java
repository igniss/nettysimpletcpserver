package netty.tcp.server.protocol.data.heartbeat;

import netty.tcp.server.protocol.data.ResponseData;
import netty.tcp.server.protocol.enums.ProtocolType;

public class ResponseHeartbeatData extends ResponseData {

    public ResponseHeartbeatData(short serialNumber) {
        super(ProtocolType.CLASSIC_HEADER, (short) ProtocolType.HEARTBEAT.getResponsePacketLength(),
                ProtocolType.HEARTBEAT, serialNumber);
    }

}