package netty.tcp.server.protocol.data.heartbeat;


import netty.tcp.server.protocol.data.RequestData;
import netty.tcp.server.protocol.enums.GPSSignalType;
import netty.tcp.server.protocol.enums.ProtocolType;

public class RequestHeartbeatData extends RequestData {

    private final boolean charging;
    private final boolean gps;
    private final boolean defense;
    private final double voltage;
    private final GPSSignalType signalType;

    private final short sentSerialNumber;

    public RequestHeartbeatData(short length, short serialNumber, short sentSerialNumber, short errorCheck, boolean charging, boolean gps, boolean defense, double voltage, GPSSignalType signalType) {
        super(ProtocolType.CLASSIC_HEADER, ProtocolType.HEARTBEAT, length, serialNumber, errorCheck);
        this.charging = charging;
        this.gps = gps;
        this.defense = defense;
        this.voltage = voltage;
        this.signalType = signalType;
        this.sentSerialNumber = sentSerialNumber;
    }

    public boolean isCharging() {
        return charging;
    }

    public boolean isGps() {
        return gps;
    }

    public boolean isDefense() {
        return defense;
    }

    public double getVoltage() {
        return voltage;
    }

    public GPSSignalType getSignalType() {
        return signalType;
    }

    @Override
    public String toString() {
        return "RequestHeartbeatData{" +
                "serialNumber=" + getSerialNumber() +
                ", sentSerialNumber=" + sentSerialNumber +
                ", charging=" + charging +
                ", gps=" + gps +
                ", defense=" + defense +
                ", voltage=" + voltage +
                ", signalType=" + signalType +
                '}';
    }
}
