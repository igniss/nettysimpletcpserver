package netty.tcp.server.protocol.data.command;

import netty.tcp.server.protocol.data.ResponseData;
import netty.tcp.server.protocol.enums.ProtocolType;

public class ResponseCommandData extends ResponseData {

    private final String command;

    public ResponseCommandData(short serialNumber, String command) {
        super(ProtocolType.CLASSIC_HEADER, (short)(ProtocolType.COMMAND.getResponsePacketLength() + command.length()),
                ProtocolType.COMMAND, serialNumber);
        this.command = command;
    }

    public String getCommand() {
        return command;
    }

    @Override
    public String toString() {
        return "ResponseCommandData{" +
                "command='" + command + '\'' +
                '}';
    }
}