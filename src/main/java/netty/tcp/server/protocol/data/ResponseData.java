package netty.tcp.server.protocol.data;

import netty.tcp.server.protocol.enums.ProtocolType;

public class ResponseData {

    private final short header;
    private final short length;
    private final ProtocolType protocol;
    private final short serialNumber;

    public ResponseData(short header, short length, ProtocolType protocol, short serialNumber) {
        this.header = header;
        this.length = length;
        this.protocol = protocol;
        this.serialNumber = serialNumber;
    }

    public short getHeader() {
        return header;
    }

    public short getLength() {
        return length;
    }

    public ProtocolType getProtocol() {
        return protocol;
    }

    public short getSerialNumber() {
        return serialNumber;
    }

    @Override
    public String toString() {
        return "ResponseData{" +
                "header=" + header +
                ", length=" + length +
                ", protocol=" + protocol.name() + " (" + protocol.getProtocolId() + ")" +
                ", serialNumber=" + serialNumber + "}";
    }
}
