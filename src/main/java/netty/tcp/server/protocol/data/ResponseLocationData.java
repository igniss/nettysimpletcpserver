package netty.tcp.server.protocol.data;

import netty.tcp.server.protocol.enums.ProtocolType;

public class ResponseLocationData extends ResponseData {

    public ResponseLocationData(short serialNumber) {
        super(ProtocolType.EXTENDED_HEADER, (short) ProtocolType.LOCATION.getResponsePacketLength(),
                ProtocolType.LOCATION, serialNumber);
    }

}