package netty.tcp.server.protocol.data.login;
import netty.tcp.server.protocol.enums.ProtocolType;
import netty.tcp.server.protocol.data.ResponseData;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class ResponseLoginData extends ResponseData {

    private final LocalDateTime dateTime;

    public ResponseLoginData(short serialNumber) {
        super(ProtocolType.CLASSIC_HEADER, (short) ProtocolType.LOGIN.getResponsePacketLength(),
                ProtocolType.LOGIN, serialNumber);
        this.dateTime = LocalDateTime.now();
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    @Override
    public String toString() {
        return "ResponseLoginData{" +
                "dateTime=" + dateTime.format(DateTimeFormatter.ISO_DATE_TIME) +
                '}';
    }
}