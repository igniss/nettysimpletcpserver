package netty.tcp.server.protocol.data.command;

import netty.tcp.server.protocol.data.RequestData;
import netty.tcp.server.protocol.enums.ProtocolType;

public class RequestCommandData extends RequestData {

    private final String command;

    public RequestCommandData(short length, short serialNumber, short errorCheck, String command) {
        super(ProtocolType.EXTENDED_HEADER, ProtocolType.COMMAND_RESPONSE, length, serialNumber, errorCheck);
        this.command = command;
    }

    public String getCommand() {
        return command;
    }
}
