package netty.tcp.server.protocol.data.login;


import netty.tcp.server.protocol.enums.ProtocolType;
import netty.tcp.server.protocol.data.RequestData;

public class RequestLoginData extends RequestData {

    private final String imei;
    private final short model;

    public RequestLoginData(short length, short serialNumber, short errorCheck, String imei, short model) {
        super(ProtocolType.CLASSIC_HEADER, ProtocolType.LOGIN, length, serialNumber, errorCheck);
        this.imei = imei;
        this.model = model;
    }

    public String getImei() {
        return imei;
    }

    public short getModel() {
        return model;
    }

    @Override
    public String toString() {
        return "RequestLoginData{" +
                "serialNumber=" + getSerialNumber() +
                ", imei='" + imei + '\'' +
                ", model=" + model +
                '}';
    }
}
