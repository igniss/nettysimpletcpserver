package netty.tcp.server.protocol.data.alarm;

import netty.tcp.server.protocol.data.ResponseData;
import netty.tcp.server.protocol.enums.ProtocolType;

public class ResponseAlarmData extends ResponseData {

    public ResponseAlarmData(short serialNumber) {
        super(ProtocolType.EXTENDED_HEADER, (short) ProtocolType.ALARM.getResponsePacketLength(),
                ProtocolType.ALARM, serialNumber);
    }

}