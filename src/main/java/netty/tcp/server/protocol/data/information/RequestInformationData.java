package netty.tcp.server.protocol.data.information;


import netty.tcp.server.protocol.enums.ProtocolType;
import netty.tcp.server.protocol.data.RequestData;

public class RequestInformationData extends RequestData {

    private final String imei;
    private final boolean light;
    private final boolean rearLight;
    private final boolean charging;
    private final boolean braking;
    private final double speed;
    private final boolean disabled;
    private final double batteryVoltage;
    private final int batteryPercentage;
    private final double currentOutputVoltage;
    private final double batteryBalance;

    public RequestInformationData(short length, short serialNumber,
                                  short errorCheck, String imei, boolean light, boolean rearLight,
                                  boolean charging, boolean braking, double speed, boolean disabled,
                                  double batteryVoltage, int batteryPercentage, double currentOutputVoltage, double batteryBalance) {
        super(ProtocolType.EXTENDED_HEADER, ProtocolType.INFORMATION, length, serialNumber, errorCheck);
        this.imei = imei;
        this.light = light;
        this.rearLight = rearLight;
        this.charging = charging;
        this.braking = braking;
        this.speed = speed;
        this.disabled = disabled;
        this.batteryVoltage = batteryVoltage;
        this.batteryPercentage = batteryPercentage;
        this.currentOutputVoltage = currentOutputVoltage;
        this.batteryBalance = batteryBalance;
    }

    public String getImei() {
        return imei;
    }

    public boolean isLight() {
        return light;
    }

    public boolean isRearLight() {
        return rearLight;
    }

    public boolean isCharging() {
        return charging;
    }

    public boolean isBraking() {
        return braking;
    }

    public double getSpeed() {
        return speed;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public double getBatteryVoltage() {
        return batteryVoltage;
    }

    public int getBatteryPercentage() {
        return batteryPercentage;
    }

    public double getCurrentOutputVoltage() {
        return currentOutputVoltage;
    }

    public double getBatteryBalance() {
        return batteryBalance;
    }

    @Override
    public String toString() {
        return "RequestInformationData{" +
                "imei='" + imei + '\'' +
                ", light=" + light +
                ", rearLight=" + rearLight +
                ", charging=" + charging +
                ", braking=" + braking +
                ", speed=" + speed +
                ", disabled=" + disabled +
                ", batteryVoltage=" + batteryVoltage +
                ", batteryPercentage=" + batteryPercentage +
                ", currentOutputVoltage=" + currentOutputVoltage +
                ", batteryBalance=" + batteryBalance +
                '}';
    }
}
