package netty.tcp.server.protocol.data;

import netty.tcp.server.protocol.enums.ProtocolType;

public class RequestLocationData extends RequestData {

    private final float latitude;
    private final float longitude;
    private final int speed;

    public RequestLocationData(short length, short serialNumber, short errorCheck, float latitude, float longitude, int speed) {
        super(ProtocolType.EXTENDED_HEADER, ProtocolType.LOCATION, length, serialNumber, errorCheck);
        this.latitude = latitude;
        this.longitude = longitude;
        this.speed = speed;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public int getSpeed() {
        return speed;
    }

    @Override
    public String toString() {
        return "RequestLocationData{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                ", speed=" + speed +
                '}';
    }
}
