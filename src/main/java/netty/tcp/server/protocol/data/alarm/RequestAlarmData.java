package netty.tcp.server.protocol.data.alarm;

import netty.tcp.server.protocol.enums.AlarmType;
import netty.tcp.server.protocol.enums.ProtocolType;
import netty.tcp.server.protocol.data.RequestData;

public class RequestAlarmData extends RequestData {

    private final AlarmType type;

    public RequestAlarmData(short length, short serialNumber, short errorCheck, AlarmType type) {
        super(ProtocolType.EXTENDED_HEADER, ProtocolType.ALARM, length, serialNumber, errorCheck);
        this.type = type;
    }

    public AlarmType getType() {
        return type;
    }
}
