package netty.tcp.server.protocol.data;

import netty.tcp.server.protocol.enums.ProtocolType;

public class RequestData {

    private final short header;
    private final ProtocolType protocol;
    private final short length;
    private final short serialNumber;
    private final short errorCheck;

    public RequestData(short header, ProtocolType protocol, short length, short serialNumber, short errorCheck) {
        this.header = header;
        this.protocol = protocol;
        this.length = length;
        this.serialNumber = serialNumber;
        this.errorCheck = errorCheck;
    }

    public short getHeader() {
        return header;
    }

    public ProtocolType getProtocol() {
        return protocol;
    }

    public short getLength() {
        return length;
    }

    public short getSerialNumber() {
        return serialNumber;
    }

    public short getErrorCheck() {
        return errorCheck;
    }

    @Override
    public String toString() {
        return "RequestData{" +
                "header=" + header +
                ", protocol=" + protocol.name() + " (" + protocol.getProtocolId() + ")" +
                ", length=" + length +
                ", serialNumber=" + serialNumber +
                ", errorCheck=" + errorCheck +
                '}';
    }
}
