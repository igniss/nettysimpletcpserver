package netty.tcp.server;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import netty.tcp.server.protocol.ResponseEncoder;
import netty.tcp.server.protocol.data.RequestData;
import netty.tcp.server.protocol.RequestDecoder;
import netty.tcp.server.protocol.data.RequestLocationData;
import netty.tcp.server.protocol.data.ResponseLocationData;
import netty.tcp.server.protocol.data.alarm.RequestAlarmData;
import netty.tcp.server.protocol.data.alarm.ResponseAlarmData;
import netty.tcp.server.protocol.data.command.RequestCommandData;
import netty.tcp.server.protocol.data.heartbeat.RequestHeartbeatData;
import netty.tcp.server.protocol.data.heartbeat.ResponseHeartbeatData;
import netty.tcp.server.protocol.data.information.RequestInformationData;
import netty.tcp.server.protocol.data.information.ResponseInformationData;
import netty.tcp.server.protocol.data.login.RequestLoginData;
import netty.tcp.server.protocol.data.login.ResponseLoginData;

import java.nio.ByteBuffer;
import java.util.HashMap;


public class SimpleTCPChannelHandler extends SimpleChannelInboundHandler<String> {

    public static final HashMap<Channel, Short> scooterSerialNumbers = new HashMap<>();

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
        Util.log(ctx.channel().remoteAddress(), "Channel Active");
    }

    @Override
    public void channelRead0(ChannelHandlerContext ctx, String str) {
        ByteBuffer wrap = ByteBuffer.wrap(str.getBytes());
        ByteBuf byteBuf = Unpooled.wrappedBuffer(wrap);

        RequestData requestData = RequestDecoder.decode(ctx, byteBuf);
        if (requestData == null) {
            //Util.log(ctx.channel().remoteAddress(), "Invalid request.");
            return;
        }

        if (requestData instanceof RequestLoginData) {
            this.handleLoginData(ctx, (RequestLoginData) requestData);
        } else if (requestData instanceof RequestHeartbeatData) {
            this.handleHeartbeatData(ctx, (RequestHeartbeatData) requestData);
        } else if (requestData instanceof RequestLocationData) {
            this.handleLocationData(ctx, (RequestLocationData) requestData);
        } else if (requestData instanceof RequestInformationData) {
            this.handleInformationData(ctx, (RequestInformationData) requestData);
        } else if (requestData instanceof RequestAlarmData) {
            this.handleAlarmData(ctx, (RequestAlarmData) requestData);
        }

        Util.log(ctx.channel().remoteAddress(), requestData.toString());
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        Util.log(ctx.channel().remoteAddress(), "Channel Inactive");
    }

    private void handleLoginData(ChannelHandlerContext ctx, RequestLoginData loginData) {
        Channel channel = ctx.channel();

        //Write response login data
        ResponseLoginData responseLoginData = new ResponseLoginData(loginData.getSerialNumber());
        ByteBuf data = ResponseEncoder.encode(responseLoginData);
        ctx.channel().writeAndFlush(data);

        scooterSerialNumbers.put(channel, loginData.getSerialNumber());
    }

    public void handleHeartbeatData(ChannelHandlerContext ctx, RequestHeartbeatData heartbeatData) {
        Channel channel = ctx.channel();
        Short serialNumber = scooterSerialNumbers.get(channel);

        if (serialNumber == null) return;

        ResponseHeartbeatData responseHeartbeatData = new ResponseHeartbeatData(serialNumber);

        //Send response to scooter
        ByteBuf data = ResponseEncoder.encode(responseHeartbeatData);
        ctx.channel().writeAndFlush(data);
    }

    public void handleLocationData(ChannelHandlerContext ctx, RequestLocationData locationData) {
        ResponseLocationData responseLocationData = new ResponseLocationData(locationData.getSerialNumber());

        //Send response to scooter
        ByteBuf data = ResponseEncoder.encode(responseLocationData);
        ctx.channel().writeAndFlush(data);
    }

    public void handleAlarmData(ChannelHandlerContext ctx, RequestAlarmData alarmData) {
        ResponseAlarmData responseAlarmData = new ResponseAlarmData(alarmData.getSerialNumber());

        //Send response to scooter
        ByteBuf data = ResponseEncoder.encode(responseAlarmData);
        ctx.channel().writeAndFlush(data);
    }

    private void handleInformationData(ChannelHandlerContext ctx, RequestInformationData informationData) {
        //Write response information data
        ResponseInformationData responseInformationData = new ResponseInformationData(informationData.getSerialNumber());
        ByteBuf data = ResponseEncoder.encode(responseInformationData);
        ctx.channel().writeAndFlush(data);
    }
}
